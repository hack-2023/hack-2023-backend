using Data.Database;
using Hack2023.DataAccess.Repositories;
using Hack2023.Domain.Repositories;
using Hack2023.Features;
using Hack2023.Infrastructure.DatabaseBuilder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<ITempRepository, TempRepository>();

builder.Services.AddControllers();
builder.Services.AddDbContext<AppDbContext>(opt =>
    opt.UseNpgsql(builder.Configuration.GetConnectionString("Default")));
builder.Services.AddApiVersioning(opt =>
{
    var defaultVersion = new ApiVersion(1, 0);
    opt.AssumeDefaultVersionWhenUnspecified = false;
    opt.DefaultApiVersion = defaultVersion;
    opt.ReportApiVersions = true; 
});

builder.Services.AddHttpContextAccessor();
builder.Services.AddMediatR(cfg => { cfg.RegisterServicesFromAssembly(FeaturesAssemblyReference.Assembly); });

var app = builder.Build();

app.UpdateDb<AppDbContext>();

app.MapControllers();

app.MapGet("/", () => "Hello, world");

app.Run();

public partial class Program
{
}