﻿FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Hack2023.WebApi/Hack2023.WebApi.csproj", "Hack2023.WebApi/"]
COPY ["Hack2023.Features/Hack2023.Features.csproj", "Hack2023.Features/"]
COPY ["Hack2023.Infrastructure/Hack2023.Infrastructure.csproj", "Hack2023.Infrastructure/"]
COPY ["Hack2023.Domain/Hack2023.Domain.csproj", "Hack2023.Domain/"]
COPY ["Hack2023.Data/Hack2023.Data.csproj", "Hack2023.Data/"]
COPY ["Hack2023.DataAccess/Hack2023.DataAccess.csproj", "Hack2023.DataAccess/"]

RUN dotnet restore "Hack2023.WebApi/Hack2023.WebApi.csproj"

COPY ["./Hack2023.WebApi", "/Hack2023.WebApi"]
COPY ["./Hack2023.Features", "/Hack2023.Features"]
COPY ["./Hack2023.Infrastructure", "/Hack2023.Infrastructure"]
COPY ["./Hack2023.Domain", "/Hack2023.Domain"]
COPY ["./Hack2023.Data", "/Hack2023.Data"]
COPY ["./Hack2023.DataAccess", "/Hack2023.DataAccess"]

WORKDIR "/Hack2023.WebApi"
RUN dotnet build "Hack2023.WebApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Hack2023.WebApi.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final

RUN addgroup --system nonroot  \
    && adduser --system --group nonroot

USER nonroot

WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Hack2023.WebApi.dll"]
