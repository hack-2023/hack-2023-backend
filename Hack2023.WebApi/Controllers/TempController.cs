﻿using Hack2023.Features.Temp;
using Hack2023.Features.Temp.Queries.FindTempById;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace hack_2023_back.Controllers;

[Route("v{version:apiVersion}/[controller]")]
public class TempController : Controller
{
    private readonly IMediator _mediator;

    public TempController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async Task<IActionResult> GetTemp(CancellationToken cancellationToken = default)
    {
        var result = await _mediator.Send(new TempQuery(), cancellationToken);

        return result.IsSuccess ? Ok(result.Value) : BadRequest();
    }

    [HttpGet, Route("findById")]
    public async Task<IActionResult> GetTempById([FromQuery] int tempId, CancellationToken cancellationToken = default)
    {
        try
        {
            var result = await _mediator.Send(new FindTempByIdQuery(tempId), cancellationToken);

            return result.IsSuccess ? Ok(result.Value) : BadRequest(result.Error);
        }
        catch (Exception ex)
        {
            return Ok(ex.Message);
        }
    }
}