﻿using Hack2023.Infrastructure.Cqrs;
using Hack2023.Infrastructure.Cqrs.Queries;

namespace Hack2023.Features.Temp;

public class TempQueryHandler : IQueryHandler<TempQuery, string>
{
    public async Task<Result<string>> Handle(TempQuery request, CancellationToken cancellationToken)
    {
        await Task.Delay(100, cancellationToken);

        return new Result<string>(val: "asd", true);
    }
}