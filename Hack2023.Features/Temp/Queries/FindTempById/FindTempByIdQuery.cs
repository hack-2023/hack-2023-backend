﻿using Hack2023.Infrastructure.Cqrs.Queries;

namespace Hack2023.Features.Temp.Queries.FindTempById;

public class FindTempByIdQuery : IQuery<Domain.Entities.Temp>
{
    public FindTempByIdQuery(int id)
    {
        Id = id;
    }

    public int Id { get; init; }
}