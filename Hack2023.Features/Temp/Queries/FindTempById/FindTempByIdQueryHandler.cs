﻿using Hack2023.Domain.Repositories;
using Hack2023.Infrastructure.Cqrs;
using Hack2023.Infrastructure.Cqrs.Queries;

namespace Hack2023.Features.Temp.Queries.FindTempById;

public class FindTempByIdQueryHandler : IQueryHandler<FindTempByIdQuery, Domain.Entities.Temp>
{
    private readonly ITempRepository _tempRepository;

    public FindTempByIdQueryHandler(ITempRepository tempRepository)
    {
        _tempRepository = tempRepository;
    }

    public async Task<Result<Domain.Entities.Temp>> Handle(FindTempByIdQuery request,
        CancellationToken cancellationToken)
    {
        var result = await _tempRepository.FindByIdAsync(request.Id, cancellationToken: cancellationToken);

        return result is null
            ? new Result<Domain.Entities.Temp>(result, false, $"Объект по id = {request.Id} не найден")
            : new Result<Domain.Entities.Temp>(result, true);
    }
}