﻿using System.Reflection;

namespace Hack2023.Features;

public static class FeaturesAssemblyReference
{
    public static readonly Assembly Assembly = typeof(FeaturesAssemblyReference).Assembly;
}