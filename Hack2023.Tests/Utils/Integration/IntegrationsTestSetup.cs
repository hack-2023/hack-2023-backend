﻿namespace Hack2023.Test.Utils.Integration;

public class IntegrationsTestSetup
{
    internal static readonly HttpClient TestClient;
    private static readonly CustomWebApplicationFactory<Program> _factory;

    static IntegrationsTestSetup()
    {
        _factory ??= new CustomWebApplicationFactory<Program>();
        TestClient ??= _factory.CreateClient();
    }

}