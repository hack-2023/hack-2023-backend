﻿using Data.Database;
using Hack2023.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Hack2023.Test.Utils.Unit;

public class MockDatabaseForUnitTests
{
    private readonly DbContextOptions<AppDbContext> _dbContextOptions = new DbContextOptionsBuilder<AppDbContext>()
        .UseInMemoryDatabase(databaseName: "TempDatabase")
        .Options;

    protected AppDbContext _appDbContext;

    public MockDatabaseForUnitTests()
    {
        _appDbContext = new AppDbContext(_dbContextOptions);
        _appDbContext.Database.EnsureCreated();

        SeedDatabase();
    }

    private void SeedDatabase()
    {
        _appDbContext.Temps.AddRange(new[]
        {
            new Temp { Id = 1 },
            new Temp { Id = 2 },
        });
    }
}