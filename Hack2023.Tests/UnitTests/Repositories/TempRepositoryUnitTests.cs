using Hack2023.Test.Utils.Unit;
using Xunit;

namespace Hack2023.Test.UnitTests.Repositories;

public class TempRepositoryUnitTests : MockDatabaseForUnitTests
{

    [Fact]
    public void Check_If_Temp_Database_Created()
    {
        var hasAnyValue = _appDbContext.Temps.Any();
        
        Assert.True(hasAnyValue);
    }
    
}