﻿using System.Net;
using System.Text.Json;
using Hack2023.Domain.Entities;
using Hack2023.Test.Utils.Integration;
using Xunit;
using Xunit.Abstractions;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Hack2023.Test.IntegrationTests.Queries;

public class FindTempByIdQueryTests : IntegrationsTestSetup
{
    [Fact]
    public async Task Temp_Get_Query()
    {
        var response = await TestClient.GetAsync("/");
        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
    }

    [Fact]
    public async Task Temp_Data_Id_1_Is_Exists()
    {
        var response = await TestClient.GetAsync("/Temp/findById?tempId=1");

        var jsonResult = await response.Content.ReadAsStringAsync();

        Temp? result = null;

        result = JsonSerializer.Deserialize<Temp>(jsonResult, options: new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        });


        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        Assert.NotNull(result);
        Assert.Equal(1, result!.Id);
    }
}