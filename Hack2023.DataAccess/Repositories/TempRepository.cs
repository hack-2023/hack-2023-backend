﻿using Data.Database;
using Hack2023.Domain.Entities;
using Hack2023.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Hack2023.DataAccess.Repositories;

public class TempRepository : ITempRepository
{
    private readonly AppDbContext _appDbContext;

    public TempRepository(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public async Task<Temp?> FindByIdAsync(int id, CancellationToken cancellationToken) =>
        await _appDbContext.Temps.FirstOrDefaultAsync(x => x.Id == id, cancellationToken: cancellationToken);
}