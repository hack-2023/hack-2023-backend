﻿using Hack2023.Domain.Entities;

namespace Hack2023.Domain.Repositories;

public interface ITempRepository
{
    public Task<Temp?> FindByIdAsync(int id, CancellationToken cancellationToken);
}