using MediatR;

namespace Hack2023.Infrastructure.Cqrs.Queries;

public interface IQuery<TResponse>  : IRequest<Result<TResponse>>
{
}