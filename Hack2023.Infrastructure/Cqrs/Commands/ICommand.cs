using MediatR;

namespace Hack2023.Infrastructure.Cqrs.Commands;

public interface ICommand : IRequest<Result>
{
}

public interface ICommand<TResponse> : IRequest<Result<TResponse>>
{
}