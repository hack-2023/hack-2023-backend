using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Hack2023.Infrastructure.DatabaseBuilder
{
    public static class DatabaseBuilderExtensions
    {
        public static void UpdateDb<TContext>(this IApplicationBuilder app) where TContext : DbContext
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetService<TContext>();

            if (context is null)
                throw new Exception("Экземпляр базы данных не найден");

            if (context.Database.IsRelational())
                context.Database.Migrate();
        }
    }
}