﻿using Hack2023.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Data.Database;

public class AppDbContext : DbContext
{
    public AppDbContext()
    {
    }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<Temp>().HasData(new[]
        {
            new Temp { Id = 1 },
            new Temp { Id = 2 },
        });
    }

    public DbSet<Temp> Temps { get; set; }
}